## Run locally

1. Build your container
```shell
apptainer build --fakeroot nvidia_tensorflow.sif nvidia_tensorflow.def
```

2. Run

- Run test line to see that correct tf version is installed:

```shell
apptainer exec nvidia_tensorflow.sif python -c "import tensorflow as tf; print("TensorFlow version:", tf.__version__)"
```

- Or run training of MNIST:

```shell
apptainer exec --fakeroot nvidia_tensorflow.sif python MNIST_example/src/main.py --task train --model_name model_distr --num_epochs 5 --batch_size 500 --do_profile
```

## Run on RAVEN

1. Load the apptainer modules:

```shell
module purge #this will unload all previous modules
module load apptainer/1.2.2
```

2. Build your container:

```shell
apptainer build --fakeroot nvidia_tensorflow.sif nvidia_tensorflow.def
```

3. Run

3.1. You could now test that correct version of  container is installed:

```shell
apptainer exec --nv nvidia_tensorflow.sif python -c "import tensorflow as tf; print(f'TensorFlow version:{tf.__version__}')"
```

Alternatively, you could connect to container shell and test that everything withing a container is visible/runs correcly: 

```shell
apptainer shell nvidia_tensorflow.sif #This will switch you to the container instance
python -c "import tensorflow as tf; print(f'TensorFlow version:{tf.__version__}')"
```

3.2. You could run training of MNIST via command:

```shell
apptainer exec --nv nvidia_tensorflow.sif python MNIST_example/src/main.py --task train --model_name test --num_epochs 5 --batch_size 100
```

See example of submitting a job to the RAVEN cluster: 
- **For undistributed training** : in "scripts/run_undistributed.slurm"
- **For distributed training** using 1 node and 4 GPUs: in "scripts/run_distributed_4_gpu.slurm"

## Monitor training

One can connect to tensorboard to monitor training.
In order to do so:

1. On you local machine run:

```shell
ssh -N -f -L localhost:6006:localhost:6006 user.name@raven.mpcdf.mpg.de 
```

2. On the cluster, load tensorboard module:

```shell
module load anaconda/3/2021.11 tensorflow/cpu/2.11.0 tensorboard/2.11.0
```

3. Connect to your log directory:


```shell
tensorboard --logdir MNIST_example/models/test/logs --port 6006
```

4. Now in a browser, open http://localhost:6006