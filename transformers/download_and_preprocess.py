import argparse
import requests

from datasets import Dataset
from transformers import AutoTokenizer, AutoModelForCausalLM


def main(args):
    input_file = requests.get(
        "https://raw.githubusercontent.com/karpathy/char-rnn/master/data/tinyshakespeare/input.txt"
    )
    open("./input.txt", "wb").write(input_file.content)

    ds = Dataset.from_text("./input.txt")
    ds = ds.train_test_split(test_size=0.2)

    # this downloads the pretrained weights and caches them
    model = AutoModelForCausalLM.from_pretrained("distilgpt2")
    tokenizer = AutoTokenizer.from_pretrained(args.model_id)

    def tokenize(examples):
        return tokenizer(examples["text"])

    tokenized_ds = ds.map(tokenize, batched=True, remove_columns=["text"])

    def chunk_text(examples):
        chunk_size = 128
        # Concatenate tokens of the batch
        examples = {key: sum(examples[key], []) for key in examples}
        # Split tokens into chunks of chunk_size
        # We will drop the remainder of size < chunk_size
        total_length = len(examples["input_ids"])
        max_length = (total_length // chunk_size) * chunk_size
        examples = {
            key: [value[i : i + chunk_size] for i in range(0, max_length, chunk_size)]
            for key, value in examples.items()
        }
        examples["labels"] = examples["input_ids"].copy()

        return examples

    lm_ds = tokenized_ds.map(chunk_text, batched=True)

    lm_ds.save_to_disk(args.output_dir)


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "--output_dir",
        "-o",
        type=str,
        default="./dataset",
        help="Where to save the preprocessed dataset?",
    )
    parser.add_argument(
        "--model_id",
        "-m",
        type=str,
        default="distilgpt2",
        help="Model ID in the HuggingFace Hub.",
    )
    args = parser.parse_args()

    main(args)
