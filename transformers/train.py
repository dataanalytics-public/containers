# Reference: https://huggingface.co/docs/transformers/tasks/language_modeling
import argparse
import math

from datasets import DatasetDict
from transformers import AutoTokenizer
from transformers import DataCollatorForLanguageModeling
from transformers import AutoModelForCausalLM, TrainingArguments, Trainer


def main(args):
    model = AutoModelForCausalLM.from_pretrained("distilgpt2")
    tokenizer = AutoTokenizer.from_pretrained("distilgpt2")

    ds = DatasetDict.load_from_disk(args.data_path)

    tokenizer.pad_token = tokenizer.eos_token
    data_collator = DataCollatorForLanguageModeling(tokenizer=tokenizer, mlm=False)

    training_args = TrainingArguments(
        output_dir="model",
        learning_rate=args.lr,
        weight_decay=0.01,
        per_device_train_batch_size=args.bs,
        evaluation_strategy="epoch",
        ddp_find_unused_parameters=False,
        log_level="info",
        log_level_replica="warning",
        log_on_each_node=False,
    )

    trainer = Trainer(
        model=model,
        args=training_args,
        train_dataset=ds["train"],
        eval_dataset=ds["test"],
        data_collator=data_collator,
    )

    trainer.train()

    eval_results = trainer.evaluate()
    print(f"Perplexity: {math.exp(eval_results['eval_loss']):.2f}")


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "--data_path",
        type=str,
        default="./dataset",
        help="Path to the preprocessed dataset.",
    )
    parser.add_argument("--lr", type=float, default=2e-5, help="The learning rate.")
    parser.add_argument(
        "--bs", type=int, default=8, help="The batch size per device during training."
    )
    args = parser.parse_args()

    main(args)
