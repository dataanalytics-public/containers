# transformers

This container uses Nvidia's PyTorch as base layer and adds Hugging Face's [transformers](https://github.com/huggingface/transformers), [accelerate](`https://github.com/huggingface/accelerate`) and [datasets](https://github.com/huggingface/datasets).

## Build
The `transformers` container is built on top of the `nvidia_pytorch` container:

```shell
apptainer pull nvidia_pytorch.sif docker://nvcr.io/nvidia/pytorch:24.02-py3
apptainer build transformers.sif transformers.def
```

## Run

Download the pre-trained weights and the dataset, and preprocess it:
```shell
apptainer exec transformers.sif python download_and_preprocess.py
```

Train the model:
```shell
apptainer exec --nv transformers.sif python train.py
```

You can also create a [Jupyter kernel](../README.md#2-setting-up-rvs) with the `transformers.sif` container and experiment with the `develop.ipynb` notebook.
For this, just adapt the `kernel.json` file and copy it to your `kernels` folder:
```shell
kernel_folder=~/.local/share/jupyter/kernels/transformers_container
mkdir -p $kernel_folder && cp kernel.json $kernel_folder
```
Then run Jupyter and select the `transformers (container)` kernel.

## Example

The `distributed_training.slurm` script showcase how to perform a multi-node multi-GPU training with [Hugging Face's transformers](https://github.com/huggingface/transformers) library.
Here we will fine-tune a distilled version of GPT2 on a small Shakespear dataset, using 2 nodes with 4 GPUs each.

The transformers [`Trainer`](https://huggingface.co/docs/transformers/main_classes/trainer) (together with [accelerate](https://github.com/huggingface/accelerate)) and the [`torchrun`](https://pytorch.org/docs/stable/elastic/run.html) command automates almost everything.
We can use the same python script to run a single-device training on our laptop/machine, or a multi-device training on our SLURM cluster.

To run the example locally on your machine, first get the Shakespear data and tokenize/chunk it:
```shell
apptainer exec transformers.sif python download_and_preprocess.py
```
Then run the training:
```shell
# You can leave out the `--nv` flag if you do not have a NVIDIA GPU
apptainer exec --nv transformers.sif python train.py
```

To run a multi-node multi-GPU training on our SLURM cluster, we just need to specify the resources in the SLURM script:
```shell
#SBATCH --nodes=2  # <- Number of nodes we want to use for the training
#SBATCH --tasks-per-node=1  # <- Has to be 1, since torchrun takes care of spawning the processes for each GPU
#SBATCH --gres=gpu:a100:4  # <- Number of GPUs you want to use per node, here 4
```

Send the job to the queue:
```shell
sbatch distributed_training.slurm
```

After the training, you can find the fine-tuned model in the `./model` folder.

## Notes
- Be aware of HuggingFace's caching mechanisms when running a multi-gpu training. Maybe download the models/datasets first and instantiate the respective classes with the files directly to avoid concurrent downloads of the same files.
- Data should be read from the faster `/ptmp` partition. Make sure the cached datasets from Hugging Face's [datasets](https://github.com/huggingface/datasets) use this partition.
